Container exec
==============

Runs a command inside a container.

Role Variables
--------------

* `name`: Container name
* `command`: Command

Example Playbook
----------------

```yaml
- name: "Nginx reload"
  include_role:
    name: "sutty-container-exec"
  vars:
    name: "nginx"
    command: "nginx -s reload"
```

License
-------

MIT-Antifa
